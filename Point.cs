﻿namespace ShapeSerializerHW
{
    internal class Point : IFigure
    {
        public readonly int X, Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public string Export(IExport export)
            => export.Export(this);
    }
}
