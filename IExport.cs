﻿namespace ShapeSerializerHW
{
    internal interface IExport
    {
        string Export(Point point);
        string Export(Circle circle);
        string Export(Square square);
    }
}
