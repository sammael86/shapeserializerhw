﻿namespace ShapeSerializerHW
{
    internal class Circle : IFigure
    {
        public readonly int X, Y, R;

        public Circle(int x, int y, int r)
        {
            X = x;
            Y = y;
            R = r;
        }

        public string Export(IExport export)
            => export.Export(this);
    }
}
