﻿namespace ShapeSerializerHW
{
    internal class ExportJson : IExport
    {
        public string Export(Point point) =>
            $@"{{""x"": {point.X}, ""y"": {point.Y}}}";

        public string Export(Circle circle) =>
            $@"{{""x"": {circle.X}, ""y"": {circle.Y}, ""r"":{circle.R}}}";

        public string Export(Square square) =>
            $@"{{""x"": {square.X}, ""y"": {square.Y}, ""w"":{square.W}}}";
    }
}
