﻿namespace ShapeSerializerHW
{
    internal class Square : IFigure
    {
        public readonly int X, Y, W;

        public Square(int x, int y, int w)
        {
            X = x;
            Y = y;
            W = w;
        }

        public string Export(IExport export)
            => export.Export(this);
    }
}
