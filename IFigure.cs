﻿namespace ShapeSerializerHW
{
    internal interface IFigure
    {
        string Export(IExport export);
    }
}
