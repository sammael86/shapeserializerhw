﻿using System;

namespace ShapeSerializerHW
{
    internal class Program
    {
        private static void Main()
        {
            IFigure point = new Point(30, 70);
            IFigure circle = new Circle(30, 70, 10);
            IFigure square = new Square(30, 70, 15);

            IExport exportXml = new ExportXml();
            IExport exportJson = new ExportJson();
            Context context = new Context();

            context.SetExport(exportXml);
            Console.WriteLine(context.DoExport(point));
            Console.WriteLine(context.DoExport(circle));
            Console.WriteLine(context.DoExport(square));

            context.SetExport(exportJson);
            Console.WriteLine($"point: {context.DoExport(point)}");
            Console.WriteLine($"circle: {context.DoExport(circle)}");
            Console.WriteLine($"square: {context.DoExport(square)}");
        }
    }
}
