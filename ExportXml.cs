﻿namespace ShapeSerializerHW
{
    internal class ExportXml : IExport
    {
        public string Export(Point point) =>
$@"<POINT>
    <X>{point.X}</X>
    <Y>{point.Y}</Y>
</POINT>";

        public string Export(Circle circle) =>
$@"<CIRCLE>
    <X>{circle.X}</X>
    <Y>{circle.Y}</Y>
    <R>{circle.R}</R>
</CIRCLE>";

        public string Export(Square square) =>
$@"<SQUARE>
    <X>{square.X}</X>
    <Y>{square.Y}</Y>
    <W>{square.W}</W>
</SQUARE>";
    }
}
