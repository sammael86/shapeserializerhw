﻿namespace ShapeSerializerHW
{
    internal class Context
    {
        private IExport _export;

        public void SetExport(IExport export)
            => _export = export;

        public string DoExport(IFigure figure)
            => figure.Export(_export);
    }
}